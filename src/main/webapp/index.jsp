<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<form action="${pageContext.request.contextPath}/CorreioServico"
		method="post">
		<label>CEP:</label>
		<input type="text" name="cep"/>
		<input type="submit" value="Consultar cep" />
	</form>
	<p>
		<c:out value="${endereco}"></c:out>
	</p>
</body>
</html>