package com.fourcatsdev.correiosweb.controle;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fourcatsdev.correiosweb.servico.AtendeCliente;
import com.fourcatsdev.correiosweb.servico.EnderecoERP;
import com.fourcatsdev.correiosweb.servico.util.ConexaoCorreios;

@WebServlet("/CorreioServico")
public class CorreioServico extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private AtendeCliente atendeCliente;
  
    public CorreioServico() {
       atendeCliente = ConexaoCorreios.getServicocorreio();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		String cep = request.getParameter("cep");
		String end = "";
		try {
			EnderecoERP endereco = atendeCliente.consultaCEP(cep);
			end = endereco.getEnd() + ", " +					
					     endereco.getBairro() + ", " +
					     endereco.getCidade() + "-" + endereco.getUf();						
		} catch (Exception e) {
			end = "Endereço não existe";
		}
		
		request.setAttribute("endereco", end);
		RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
