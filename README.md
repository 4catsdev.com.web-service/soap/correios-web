# correios-web
Um exemplo de consulta de endereço de uma localidade através do CEP. A aplicação utiliza o serviço SOAP fornecido pelos Correios. Em sua página principal, a aplicação disponibiliza um formulário no qual o usuário informa o CEP e recebe como resultado o endereço correspondente. 

#### Características:
- Usa-se o core do JSTL para mostrar o endereço;
- É usado o jaxws-maven-plugin para gerar as classes dos correios no cliente;
- É usado a versão 3.0.0 do jax-ws
